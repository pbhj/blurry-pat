#!/bin/bash
# convert from PDF to TIF

for i in "tiffgray" "tifflzw" "tiffg3" "tiffg4" "tiffg32d" "tiffscaled"
do 
	ghostscript -sDEVICE=$i -r300 -dNOPAUSE -dBATCH -dDITHERPPI=60 -dDownScaleFactor=2 -sPageList=1 -o$i-d60-%d.tif "./WO2021033087-APBDY-20210225-3580_woar/P201806455-appb-000004.pdf"
done
